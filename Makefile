.EXPORT_ALL_VARIABLES:

PROJECT_NAME=linxist
COMPOSE_SERVICE_NAME?=linxist-api
COMPOSE_FILE?=docker-compose.yaml
COMPOSE_COMMAND=docker-compose -f ${COMPOSE_FILE}
PYTEST_WORKERS?=auto
TEST_COMMAND=pytest -n ${PYTEST_WORKERS}
COVERAGE_OPTIONS=--cov=${PROJECT_NAME} \
	--cov-report=xml:reports/coverage.xml \
	--junit-xml=reports/junit.xml
LINTING_OPTIONS?=--pylint \
	${PROJECT_NAME} tests \
	--pylint-rcfile=.pylintrc \
	--pylint-error-types=EF \
	--pylint-output-file=reports/pylint.report


default:
	@echo "\nTargets:\n"
	@echo "params\t\t\t\tPrint current parameters."
	@echo "test\t\t\t\tRun tests in native mode."
	@echo "test-coverage\t\t\tRun tests in native mode with code coverage."
	@echo "compose-build\t\t\tBuild Docker image."
	@echo "compose-up\t\t\tRun application in Docker container (needs to perform a build before)."
	@echo "compose-down\t\t\tShut down currently running Docker container app."
	@echo "compose-test\t\t\tRun tests in Docker container."
	@echo "compose-test-coverage\t\tRun tests in Docker container with code coverage."

params:
	@echo "\nCurrent parameters:"
	@echo "Project name: ${PROJECT_NAME}"
	@echo "Compose Service name: ${COMPOSE_SERVICE_NAME}"
	@echo "Compose file: ${COMPOSE_FILE}"
	@echo "Compose command: ${COMPOSE_COMMAND}"
	@echo "Pytest workers: ${PYTEST_WORKERS}"
	@echo "Test command: ${TEST_COMMAND}"
	@echo "Coverage options: ${COVERAGE_OPTIONS}"
	@echo "Linting options: ${LINTING_OPTIONS}"

test:
	@${TEST_COMMAND} ${LINTING_OPTIONS}

test-coverage:
	@${TEST_COMMAND} ${LINTING_OPTIONS} ${COVERAGE_OPTIONS}

compose-build:
	@${COMPOSE_COMMAND} build ${COMPOSE_SERVICE_NAME}

compose-up:
	@${COMPOSE_COMMAND} up -d ${COMPOSE_SERVICE_NAME}

compose-logs:
	@${COMPOSE_COMMAND} logs -f ${COMPOSE_SERVICE_NAME}

compose-down:
	@${COMPOSE_COMMAND} down --rmi local --remove-orphans

compose-test:
	@${COMPOSE_COMMAND} exec -T ${COMPOSE_SERVICE_NAME} ${TEST_COMMAND} ${LINTING_OPTIONS}

compose-test-coverage:
	@${COMPOSE_COMMAND} exec -T ${COMPOSE_SERVICE_NAME} \
		${TEST_COMMAND} \
		${LINTING_OPTIONS} \
		${COVERAGE_OPTIONS}
