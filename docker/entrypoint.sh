#!/usr/bin/env sh
set -e

echo "Waiting for PostgreSQL database..."
while ! nc -z postgresql 5432; do
  sleep 0.1
done
echo "PostgreSQL started."

echo "Updating database schema..."
alembic upgrade head
echo "Database schema is up to date."

echo "Starting app..."
uvicorn linxist.main:app --reload --host 0.0.0.0
