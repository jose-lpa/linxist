from typing import List

from fastapi import APIRouter, Depends, status

from linxist.core.dependencies import get_current_user, get_database
from linxist.core.service_result import handle_result
from linxist.models.user import User
from linxist.services.record import RecordService
from linxist.schemas.record import RecordCreate, RecordDetail


router = APIRouter(
    prefix="/records",
    tags=["records, links"],
    responses={404: {"description": "Not found"}}
)


@router.get("", response_model=List[RecordDetail])
async def get_records(db: get_database = Depends(), user: User = Depends(get_current_user)):
    result = RecordService(db).get_records()
    return handle_result(result)


@router.post("", response_model=RecordDetail, status_code=status.HTTP_201_CREATED)
async def create_record(
    record: RecordCreate,
    db: get_database = Depends(),
    user: User = Depends(get_current_user)
):
    result = RecordService(db).create_record(record)
    return handle_result(result)


@router.get("/{record_id}", response_model=RecordDetail)
async def get_record(record_id: int, db: get_database = Depends()):
    result = RecordService(db).get_record(record_id)
    return handle_result(result)
