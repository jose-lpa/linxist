from uuid import UUID

from fastapi import APIRouter, Depends, status
from fastapi.security import OAuth2PasswordRequestForm

from linxist.core.dependencies import get_database
from linxist.core.service_result import handle_result
from linxist.schemas.user import Token, UserCreate, UserDetail
from linxist.services.user import UserService


router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}}
)


@router.post("/register", response_model=UserDetail, status_code=status.HTTP_201_CREATED)
async def new_user(user: UserCreate, db: get_database = Depends()):
    result = UserService(db).create_user(user=user)
    return handle_result(result)


@router.get("/{user_id}", response_model=UserDetail)
async def detail_user(user_id: UUID, db: get_database = Depends()):
    result = UserService(db).get_user(user_id=user_id)
    return handle_result(result)


@router.post("/token", response_model=Token)
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: get_database = Depends()):
    result = UserService(db).authenticate_user(form_data.username, form_data.password)
    return handle_result(result)
