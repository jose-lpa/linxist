import sys
from pathlib import Path

from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError

from starlette.exceptions import HTTPException
from starlette.middleware.cors import CORSMiddleware


def create_app():
    sys.path.insert(0, str(Path().absolute().parent))

    # pylint: disable=import-outside-toplevel
    from linxist.core.app_exceptions import (
        app_exception_handler,
        AppExceptionCase,
        http_exception_handler,
        request_validation_exception_handler
    )
    from linxist.core.config import get_settings
    from linxist.routers import health, record, user

    settings = get_settings()

    fastapi_app = FastAPI(title=settings.PROJECT_NAME)

    @fastapi_app.exception_handler(HTTPException)
    async def custom_http_exception_handler(request, e):
        return await http_exception_handler(request, e)

    @fastapi_app.exception_handler(RequestValidationError)
    async def custom_validation_exception_handler(request, e):
        return await request_validation_exception_handler(request, e)

    @fastapi_app.exception_handler(AppExceptionCase)
    async def custom_app_exception_handler(request, e):
        return await app_exception_handler(request, e)

    fastapi_app.include_router(health.router)
    fastapi_app.include_router(user.router)
    fastapi_app.include_router(record.router, prefix=settings.API_V1_PATH)

    # Set all CORS enabled origins.
    if settings.CORS_ORIGINS:
        fastapi_app.add_middleware(
            CORSMiddleware,
            allow_origins=[str(origin) for origin in settings.CORS_ORIGINS],
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"]
        )

    return fastapi_app


app = create_app()
