from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class UserBase(BaseModel):
    first_name: Optional[str]
    last_name: Optional[str]
    email: str

    class Config:
        orm_mode = True


class UserCreate(UserBase):
    password: str


class UserDetail(UserBase):
    uuid: UUID
    created: datetime
    last_login: Optional[datetime]


class Token(BaseModel):
    token_type: str = "Bearer"
    access_token: str
