from datetime import datetime
from typing import Optional
from uuid import UUID

from pydantic import BaseModel


class RecordBase(BaseModel):
    url: str
    title: str
    description: Optional[str]
    user_uuid: UUID

    class Config:
        orm_mode = True


class RecordCreate(RecordBase):
    private_link: Optional[bool] = False


class RecordDetail(RecordBase):
    id: int
    created: datetime
    modified: Optional[datetime]
