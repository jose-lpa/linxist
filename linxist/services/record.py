from linxist.core.app_exceptions import AppException
from linxist.core.service_result import ServiceResult
from linxist.models.record import Record
from linxist.schemas.record import RecordCreate
from linxist.services.base import BaseService, BaseRepository


class RecordRepository(BaseRepository):
    model = Record


class RecordService(BaseService):
    def create_record(self, record: RecordCreate) -> ServiceResult:
        record = RecordRepository(self.db).create(record)
        if not record:
            return ServiceResult(AppException.CreateError())

        return ServiceResult(record)

    def get_record(self, record_id: int) -> ServiceResult:
        record = RecordRepository(self.db).get(field="id", value=record_id)
        if not record:
            return ServiceResult(AppException.GetError({"record_id": record_id}))

        return ServiceResult(record)

    def get_records(self) -> ServiceResult:
        records = RecordRepository(self.db).list(private_link=False, order=("id", "desc"))
        return ServiceResult(records)
