from typing import Any, List, Optional, Tuple, Type

from pydantic import BaseModel
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from linxist.core.database import Base


class DBSessionContext:
    def __init__(self, db: Session):
        self.db = db


class MetaBaseRepository(type):
    """
    Metaclass for `BaseRepository` to enforce all subclasses to follow defined patterns for
    attributes.
    """
    def __new__(mcs, name, bases, class_dict):
        model = class_dict.get("model")
        if model is not None:
            if not issubclass(model, Base):
                raise TypeError("`model` attribute must be a SQLAlchemy model.")

        return type.__new__(mcs, name, bases, class_dict)


class BaseRepository(DBSessionContext, metaclass=MetaBaseRepository):
    model: Type[Base]

    def create(self, schema: Type[BaseModel]) -> Type[Base]:
        record = self.model(**schema.dict())
        self.db.add(record)
        self.db.commit()
        self.db.refresh(record)

        return record

    def list(self, order: Tuple[str, str], **kwargs) -> List[Type[BaseModel]]:
        query = self.db.query(self.model)

        if kwargs:
            query = query.filter_by(**kwargs)

        if order and (order[1].lower() == "asc" or order[1].lower() == "desc"):
            query = query.order_by(getattr(getattr(self.model, order[0]), order[1])())

        return query.all()

    def get(self, field: str, value: Any) -> Optional[Type[Base]]:
        try:
            return self.db.query(self.model).filter(getattr(self.model, field) == value).one()
        except NoResultFound:
            return None


class BaseService(DBSessionContext):
    pass
