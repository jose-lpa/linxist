from datetime import datetime, timedelta, timezone
from uuid import UUID

from jose import jwt

from linxist.core.app_exceptions import AppException
from linxist.core.dependencies import get_settings
from linxist.core.security import pwd_context
from linxist.core.service_result import ServiceResult
from linxist.models.user import User
from linxist.schemas.user import Token, UserCreate
from linxist.services.base import BaseService, BaseRepository


class UserRepository(BaseRepository):
    model = User

    def create(self, schema: UserCreate) -> User:
        # Hash password text passed by the user to be stored in DB.
        schema.password = pwd_context.hash(schema.password)

        return super().create(schema)


class UserService(BaseService):
    def create_user(self, user: UserCreate) -> ServiceResult:
        user = UserRepository(self.db).create(user)
        if not user:
            return ServiceResult(AppException.CreateError())

        return ServiceResult(user)

    @staticmethod
    def create_access_token(uuid: UUID) -> Token:
        settings = get_settings()
        expire = datetime.now(tz=timezone.utc) + timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )
        to_encode = {"exp": expire, "sub": str(uuid)}

        token = jwt.encode(
            to_encode,
            settings.SECRET_KEY.get_secret_value(),
            algorithm=settings.ALGORITHM
        )

        return Token(access_token=token)

    def authenticate_user(self, email: str, password: str) -> ServiceResult:
        user = UserRepository(self.db).get(field="email", value=email)
        if not user or not pwd_context.verify(password, user.password):
            return ServiceResult(AppException.AuthenticationError({"user": email}))

        token = self.create_access_token(user.uuid)
        return ServiceResult(token)

    def get_user(self, user_id: UUID) -> ServiceResult:
        user = UserRepository(self.db).get(field="uuid", value=user_id)
        if not user:
            return ServiceResult(AppException.GetError({"user": str(user_id)}))

        return ServiceResult(user)
