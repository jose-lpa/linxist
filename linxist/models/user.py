from uuid import uuid4
from sqlalchemy import Boolean, Column, DateTime, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import func

from linxist.core.database import Base


class User(Base):
    __tablename__ = "linxist_user"

    uuid = Column(UUID(as_uuid=True), primary_key=True, default=uuid4)
    first_name = Column(String(50))
    last_name = Column(String(50))
    email = Column(String(254), nullable=False, unique=True)
    password = Column(String(128), nullable=False)
    active = Column(Boolean(), default=True, nullable=False)
    created = Column(DateTime(timezone=True), default=func.now(), nullable=False)
    last_login = Column(DateTime(timezone=True))

    def __repr__(self):
        return f"<User {self.uuid}: {self.email}>"
