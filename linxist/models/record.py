from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String, Text
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from linxist.core.database import Base


class Record(Base):
    __tablename__ = "record"

    id = Column(Integer, primary_key=True, autoincrement=True)
    url = Column(String(255), nullable=False)
    title = Column(String(128), nullable=False)
    description = Column(Text())
    active = Column(Boolean(), default=True, nullable=False)
    created = Column(DateTime(timezone=True), default=func.now(), nullable=False)
    modified = Column(DateTime(timezone=True), onupdate=func.now())
    private_link = Column(Boolean(), default=False, nullable=False)
    user_uuid = Column(
        UUID(as_uuid=True),
        ForeignKey("linxist_user.uuid", ondelete="CASCADE"),
        nullable=False
    )

    user = relationship("User", foreign_keys=[user_uuid])

    def __repr__(self):
        return f"<Record: {self.url} - {self.title}>"
