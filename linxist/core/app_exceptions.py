from fastapi import Request
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError

from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse
from starlette.status import HTTP_400_BAD_REQUEST


class AppExceptionCase(Exception):
    def __init__(self, status_code: int, context: dict):
        super().__init__()

        self.exception_case = self.__class__.__name__
        self.status_code = status_code
        self.context = context

    def __str__(self):
        return (
            f"<AppException {self.exception_case} - "
            + f"status_code={self.status_code} - context={self.context}>"
        )


class AppException:
    class CreateError(AppExceptionCase):
        def __init__(self, context: dict = None):
            """ Item creation failed. """
            status_code = 500
            AppExceptionCase.__init__(self, status_code, context)

    class GetError(AppExceptionCase):
        def __init__(self, context: dict = None):
            """ Item not found. """
            status_code = 404
            AppExceptionCase.__init__(self, status_code, context)

    class AuthenticationError(AppExceptionCase):
        def __init__(self, context: dict = None):
            """ Item is not public and requires auth. """
            status_code = 401
            AppExceptionCase.__init__(self, status_code, context)


async def app_exception_handler(request: Request, exc: AppExceptionCase):
    return JSONResponse(
        status_code=exc.status_code,
        content={
            "app_exception": exc.exception_case,
            "context": exc.context
        },
    )


async def http_exception_handler(request: Request, exc: HTTPException) -> JSONResponse:
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


async def request_validation_exception_handler(
    request: Request, exc: RequestValidationError
) -> JSONResponse:
    return JSONResponse(
        status_code=HTTP_400_BAD_REQUEST,
        content={"detail": jsonable_encoder(exc.errors())}
    )
