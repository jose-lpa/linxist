from typing import Generator, Union

from fastapi import Depends

from jose import jwt, JWTError
from sqlalchemy.orm import scoped_session

from linxist.core.config import get_settings
from linxist.core.database import get_database_session
from linxist.core.app_exceptions import AppException
from linxist.core.security import oauth2_scheme


def get_database() -> Generator[scoped_session, None, None]:
    ScopedSession, _ = get_database_session()
    db = ScopedSession()

    try:
        yield db
    finally:
        db.close()


async def get_current_user(token: str = Depends(oauth2_scheme)) -> dict[str, Union[int, str]]:
    """
    Checks validity of the given JWT and, if the token is valid, it returns a dictionary containing
    the expiration time of the token and the UUID of the User.
    """
    try:
        settings = get_settings()
        payload = jwt.decode(
            token, settings.SECRET_KEY.get_secret_value(), algorithms=[settings.ALGORITHM]
        )
        return payload
    except JWTError as err:
        raise AppException.AuthenticationError({"user": str(err)})
