import inspect
import logging

from linxist.core.app_exceptions import AppExceptionCase


logger = logging.getLogger(__name__)


class ServiceResult:
    def __init__(self, arg):
        if isinstance(arg, AppExceptionCase):
            self.success = False
            self.exception_case = arg.exception_case
            self.status_code = arg.status_code
        else:
            self.success = True
            self.exception_case = None
            self.status_code = None

        self.value = arg

    def __str__(self):
        if self.success:
            return "[Success]"

        return f'[Exception] "{self.exception_case}"'

    def __repr__(self):
        if self.success:
            return "<ServiceResult Success>"

        return f"<ServiceResult AppException {self.exception_case}>"

    def __enter__(self):
        return self.value

    def __exit__(self, *kwargs):
        pass


def caller_info() -> str:
    info = inspect.getframeinfo(inspect.stack()[2][0])
    return f"{info.filename}:{info.function}:{info.lineno}"


def handle_result(service_result: ServiceResult):
    if not service_result.success:
        with service_result as exception:
            logger.error("%s | caller=%s", exception, caller_info())
            raise exception

    with service_result as result:
        return result
