from typing import Tuple

from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from linxist.core.config import get_settings


def get_database_session(db_uri: str = None) -> Tuple[scoped_session, Engine]:
    if db_uri is None:
        settings = get_settings()
        engine = create_engine(settings.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
    else:
        engine = create_engine(db_uri, pool_pre_ping=True)

    session_factory = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    ScopedSession = scoped_session(session_factory)

    return ScopedSession, engine


Base = declarative_base()


def create_tables():
    _, engine = get_database_session()
    Base.metadata.create_all(bind=engine)
