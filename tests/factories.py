import factory
from factory import fuzzy

from sqlalchemy.orm import Session

from linxist.core.security import pwd_context
from linxist.models import Record, User


class BaseModelFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        abstract = True

    @classmethod
    def bind(cls, session: Session):
        cls._meta.sqlalchemy_session = session
        return cls


class UserFactory(BaseModelFactory):
    first_name = 'Test'
    last_name = factory.Sequence(lambda n: f'User {n}')
    email = factory.Sequence(lambda n: f'test-user-{n}@sharklasers.com')
    password = pwd_context.hash('testing')  # Default dummy password.

    class Meta:
        model = User
        sqlalchemy_session_persistence = 'commit'


class RecordFactory(BaseModelFactory):
    url = factory.Sequence(lambda n: f'https://{n}.patino.me')
    title = factory.Sequence(lambda n: f'Record-{n}')
    description = fuzzy.FuzzyText()

    class Meta:
        model = Record
        sqlalchemy_session_persistence = 'commit'

    @factory.lazy_attribute
    def user_uuid(self):
        """ Generate a `User` to relate to this `Record`. """
        user = UserFactory.create()
        return user.uuid.hex


class SQLModelFactory:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    @classmethod
    def create(cls, session: Session):
        return cls(create_record=RecordFactory.bind(session), create_user=UserFactory.bind(session))
