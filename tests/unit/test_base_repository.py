import pytest

from linxist.services.base import BaseRepository


def test_repository_implementation_model_attribute_type():
    """
    The subclasses of `BaseRepository` must define `model` as a SQLAlchemy declarative subclass.
    """
    # Create some dummy class to be passed as `model`. This will raise an exception.
    class DummyModel:
        pass

    with pytest.raises(TypeError) as excinfo:
        type("TestBaseRepository", (BaseRepository,), dict(model=DummyModel))

        assert "`model` attribute must be a SQLAlchemy model." in str(excinfo.value)
