import os
from typing import Generator, Tuple

import pytest

from fastapi.testclient import TestClient

from pydantic import PostgresDsn
from sqlalchemy import engine, MetaData
from sqlalchemy.engine.base import Engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy_utils import create_database, database_exists, drop_database

from linxist.core.config import get_settings
from linxist.core.database import Base, get_database_session
from linxist.core.dependencies import get_database
from linxist.main import app
from linxist.models.user import User
from linxist.services.user import UserService
from tests.factories import SQLModelFactory


def create_test_db_session() -> Tuple[scoped_session, Engine]:
    """
    Creates a database to be used by test fixtures. Database name will be the one defined in actual
    project settings, with `_test_<PROCESS_ID>` suffix, so it will support pytest parallelization.
    A testing DB will then be created for every pytest worker.
    """
    settings = get_settings()

    database_uri = PostgresDsn.build(
        scheme="postgresql",
        user=settings.POSTGRES_USER,
        password=settings.POSTGRES_PASSWORD,
        host=settings.POSTGRES_SERVER,
        path=f"/{settings.POSTGRES_DB}_test_{os.getpid()}",
    )

    return get_database_session(database_uri)


@pytest.fixture(scope="session", autouse=True)
def disposable_database() -> Generator[engine.Connection, None, None]:
    """
    Session wide fixture that creates a DB, or multiple DBs if running in parallel mode, and removes
    it at the end of tests session.

    To be used in another fixture where an existent DB session is created to be used as the real
    tests fixture. This one should not be passed to tests.
    """
    _, db_engine = create_test_db_session()

    if database_exists(db_engine.url):
        drop_database(db_engine.url)

    create_database(db_engine.url)

    with db_engine.connect() as connection:
        Base.metadata.create_all(bind=connection)
        yield connection
        Base.metadata.drop_all(bind=connection)

    drop_database(db_engine.url)


def override_get_database() -> Generator[scoped_session, None, None]:
    """
    Override function for the `get_database` FastAPI dependency, so the actual test database will be
    used instead of the app configured one.
    """
    ScopedSession, _ = create_test_db_session()
    db_session = ScopedSession()

    try:
        yield db_session
    finally:
        db_session.close()


@pytest.fixture(scope="module")
def test_client() -> Generator[TestClient, None, None]:
    """ Test client to be used to make API requests, when needed. """
    # Make sure our testing DB is used in the app too.
    app.dependency_overrides[get_database] = override_get_database

    yield TestClient(app)


@pytest.fixture(scope="function")
def test_db_session(disposable_database: engine.Connection) -> Generator["Session", None, None]:
    """
    Test DB session, automatically removes any data created in a test after test is ran. To be used
    in tests whenever access to DB is needed.
    """
    Session = sessionmaker(autocommit=False, bind=disposable_database)
    session = Session()

    try:
        yield session
    except Exception:  # pylint: disable=broad-except
        session.rollback()
    finally:
        # In any case, cleanup the test database after every test to remove any
        # created data.
        meta = MetaData()
        meta.reflect(bind=disposable_database)
        for table in meta.sorted_tables:
            session.execute(table.delete())

        session.commit()
        session.close()


@pytest.fixture
def sql_factory(test_db_session) -> Generator[SQLModelFactory, None, None]:
    """ Makes SQL models factories available in tests. """
    yield SQLModelFactory.create(test_db_session)


@pytest.fixture
def test_user_token(sql_factory) -> Generator[Tuple[User, dict[str, str]], None, None]:
    user = sql_factory.create_user()
    token = UserService.create_access_token(user.uuid)

    yield user, {"Authorization": f"{token.token_type} {token.access_token}"}
