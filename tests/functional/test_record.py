from datetime import datetime

from fastapi import status


def test_get_all_records(test_client, sql_factory, test_user_token):
    """ Test `/api/v1/records` endpoint rendering a successful response. """

    record_1 = sql_factory.create_record(modified=datetime.utcnow())
    record_2 = sql_factory.create_record()

    response = test_client.get("/api/v1/records", headers=test_user_token[1])

    assert response.status_code == status.HTTP_200_OK, response.json()

    expected = [
        {
            "created": record_2.created.isoformat(),
            "description": record_2.description,
            "id": record_2.id,
            "modified": None,
            "title": record_2.title,
            "url": record_2.url,
            "user_uuid": str(record_2.user_uuid),
        },
        {
            "created": record_1.created.isoformat(),
            "description": record_1.description,
            "id": record_1.id,
            "modified": record_1.modified.isoformat(),
            "title": record_1.title,
            "url": record_1.url,
            "user_uuid": str(record_1.user_uuid),
        },
    ]

    assert response.json() == expected


def test_get_record_detail(test_client, sql_factory):
    """ Test `/api/v1/records/{record_id}` """
    record = sql_factory.create_record()
    sql_factory.create_record()
    sql_factory.create_record()

    response = test_client.get(f"/api/v1/records/{record.id}")

    assert response.status_code == status.HTTP_200_OK

    expected = {
        "created": record.created.isoformat(),
        "description": record.description,
        "id": record.id,
        "modified": None,
        "title": record.title,
        "url": record.url,
        "user_uuid": str(record.user_uuid),
    }

    assert response.json() == expected


def test_get_record_detail_not_found(test_client):
    """ Test `/api/v1/records/{record_id}` for a non existent Record. """
    response = test_client.get("/api/v1/records/99999")

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_create_record(test_client, test_user_token):
    """ Test using `/api/v1/records` endpoint to create new record. """
    post_data = {
        "url": "https://linxist.patino.me",
        "title": "Linxist",
        "description": "App to record URLs and read them later... or not.",
        "private_link": False,
        "user_uuid": test_user_token[0].uuid.hex,
    }

    response = test_client.post("/api/v1/records", json=post_data, headers=test_user_token[1])

    assert response.status_code == status.HTTP_201_CREATED, response.json()
