import json
from datetime import datetime, timedelta, timezone
from uuid import uuid4

from fastapi import status

from freezegun import freeze_time
from jose import jwt

from linxist.core.config import get_settings
from linxist.core.security import pwd_context
from linxist.models.user import User


def test_get_user_detail(test_client, sql_factory):
    """ Test `User` detail endpoint response. """
    user = sql_factory.create_user()

    response = test_client.get(f"/users/{str(user.uuid)}")

    assert response.status_code == status.HTTP_200_OK, response.json()

    expected = {
        "uuid": str(user.uuid),
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "created": user.created.isoformat(),
        "last_login": None,
    }

    assert response.json() == expected


def test_get_user_not_found(test_client, test_db_session):
    """ API response is 404 if user is not found. """
    response = test_client.get(f"/users/{str(uuid4())}")  # Random UUID, non existing.

    assert response.status_code == status.HTTP_404_NOT_FOUND, response.json()


def test_create_user(test_client, test_db_session):
    """ Test `User` creation endpoint in a successful request. """
    post_data = {
        "first_name": "José L.",
        "last_name": "Patiño-Andrés",
        "email": "jose@sharklasers.com",
        "password": "testing-password",
    }

    response = test_client.post(
        "/users/register",
        data=json.dumps(post_data),
        headers={"Content-Type": "application/json; charset=utf8"}
    )

    assert response.status_code == status.HTTP_201_CREATED

    # Check actual API response.
    data = response.json()
    assert "uuid" in data
    assert "first_name" in data
    assert data["first_name"] == post_data["first_name"]
    assert "last_name" in data
    assert data["last_name"] == post_data["last_name"]
    assert "email" in data
    assert data["email"] == post_data["email"]
    assert "created" in data
    assert "last_login" in data

    # Check DB object was properly created.
    user = test_db_session.query(User).filter_by(email=data["email"]).one()

    assert user.first_name == post_data["first_name"]
    assert user.last_name == post_data["last_name"]
    assert user.email == post_data["email"]
    assert user.active is True
    assert user.last_login is None
    assert pwd_context.verify(post_data["password"], user.password)


def test_create_user_missing_email(test_client, test_db_session):
    """ Test `User` creation when `email` field is missing. """
    data = {
        "first_name": "José L.",
        "last_name": "Patiño Andrés",
        "password": "testing-password",
    }

    response = test_client.post(
        "/users/register",
        data=json.dumps(data),
        headers={"Content-Type": "application/json; charset=utf8"}
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    expected = {
        "detail": [
            {
                "loc": ["body", "email"],
                "msg": "field required",
                "type": "value_error.missing"
            }
        ]
    }

    assert response.json() == expected


@freeze_time("2030-12-31 23:59:59")
def test_token(test_client, sql_factory):
    """ Test user login endpoint in a successful request to get JWT token. """
    user = sql_factory.create_user()
    post_data = {"username": user.email, "password": "testing"}

    response = test_client.post(
        "/users/token",
        data=post_data,
        headers={"Content-Type": "application/x-www-form-urlencoded"}
    )

    assert response.status_code == status.HTTP_200_OK

    # Assert Bearer and refresh token has been delivered.
    data = response.json()

    assert "token_type" in data
    assert data["token_type"] == "Bearer"

    assert "access_token" in data

    settings = get_settings()
    token = jwt.decode(
        data["access_token"],
        settings.SECRET_KEY.get_secret_value(), algorithms=[settings.ALGORITHM]
    )

    expiry_time = datetime.now(tz=timezone.utc) + timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
    )

    assert token["sub"] == str(user.uuid)
    assert token["exp"] == int(expiry_time.timestamp())


def test_token_no_email(test_client, test_db_session):
    """ Test user login endpoint response when no email is provided. """
    data = {"password": "testing"}

    response = test_client.post(
        "/users/token",
        data=data,
        headers={"Content-Type": "application/x-www-form-urlencoded"}
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    expected = {
        "detail": [
            {
                "loc": ["body", "username"],
                "msg": "field required",
                "type": "value_error.missing"
            }
        ]
    }

    assert response.json() == expected, response.json()


def test_login_wrong_email(test_client, test_db_session):
    """ Test user login endpoint response when wrong email is provided. """
    data = {
        "username": "NON-EXISTENT-USER@sharklasers.com",
        "password": "testing"
    }

    response = test_client.post(
        "/users/token",
        data=data,
        headers={"Content-Type": "application/x-www-form-urlencoded"}
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED

    assert response.json() == {
        "app_exception": "AuthenticationError",
        "context": {
            "user": "NON-EXISTENT-USER@sharklasers.com"
        }
    }


def test_login_no_password(test_client, sql_factory):
    """ Test user login endpoint response when no password is provided. """
    user = sql_factory.create_user()
    data = {"username": user.email}

    response = test_client.post(
        "/users/token",
        data=data,
        headers={"Content-Type": "application/x-www-form-urlencoded"}
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST

    expected = {
        "detail": [
            {
                "loc": ["body", "password"],
                "msg": "field required",
                "type": "value_error.missing"
            }
        ]
    }

    assert response.json() == expected
