# Linxist

## Development environment setup

### Docker setup

Containers are the preferred way of running Linxist.

Using [Docker Compose](https://pypi.org/project/docker-compose/), we can build
and run Linxist like this:

    docker-compose -f docker-compose-dev.yaml build linxist-api
    docker-compose -f docker-compose-dev.yaml up linxist-api

Linxist should now be available in http://localhost:8000

#### Useful Docker commands.

- Run database migrations:

      docker-compose -f docker-compose-dev.yaml exec linxist-api alembic upgrade head

- Step into application running container:

      docker-compose -f docker-compose-dev.yaml exec linxist-api bash

### Native setup

The project can be run natively in development mode. To do this, you will need
a PostgreSQL database backend, so first please set this up.

Once you have a PostgreSQL instance ready, create a new database and set the
environment variables:

- `POSTGRES_SERVER`. Machine name and port where PostgreSQL is running. E.g.:
  `localhost:5432`
- `POSTGRES_USER`. Name of the PostgreSQL user.
- `POSTGRES_PASSWORD`. User password.
- `POSTGRES_DB`. Database name.
- `PROJECT_NAME`. The name of the project. This might be `Linxist`.
- `SECRET_KEY`. A secret key to perform cryptographic functions for user login
  and maybe another tasks. You probably can just get a nice one by typing
  `openssl rand -base64 128` in your shell.

Now, you might want to set up a Python virtual environment. Do this with your
tool of choice.

Then, just install the project development requirements with the classic Python
command:

    pip install -r requirements-dev.txt

A development database can be automatically set up using [Alembic](https://pypi.org/project/alembic/),
which is a core library of the project. Run the next command now to get a
functional, up to date database:

    alembic upgrade head

A development server can now be started by running [uvicorn](https://pypi.org/project/uvicorn/)00:

    uvicorn linxist.main:app --reload

Linxist should now be available in http://localhost:8000


## Testing

Linxist uses [pytest](https://pypi.org/project/pytest/) to run project tests,
which are placed in the `/tests` directory. You can run the tests by typing:

    docker-compose -f docker-compose-dev.yaml exec linxist-api pytest

Or simply run `pytest` if you are developing in [native mode](#native-setup).

### Coverage

Pytest can be ran with coverage plugin:

    docker-compose -f docker-compose-dev.yaml exec linxist-api pytest --cov=linxist

An XML report can be issued as well, useful for some CI systems to show metrics
in their UI:

    docker-compose -f docker-compose-dev.yaml exec linxist-api pytest --cov=linxist --cov-report=xml

This will generate a file `coverage.xml` in the root directory.

### Reports

In order to run reports with tests metrics, we can use the JUnit tool integrated
in pytest:

    docker-compose -f docker-compose-dev.yaml exec linxist-api pytest --junit-xml=junit.xml

This will generate a file `junit.xml` in the root directory.


## Database migrations

### Update database to last status

    alembic upgrade head

### Create a new migration

    alembic revision -m "Description of migration operation"

Or let Alembic automatically generate the migration file for you, if preferred:

    alembic revision --autogenerate -m "Description of migration operation"

### Upgrade to a specific database revision

    alembic upgrade <REVISION_ID>

The `<REVISION_ID>` is specified in the revision files.

### Relative migration identifiers

We can migrate by steps, both forward and backwards. Some examples:

    alembic upgrade +2
    alembic upgrade <REVISION_NUMBER>+2
    alembic downgrade -1
    ...

### Get current database revision

    alembic current

### Get database revisions history

    alembic history --verbose

## API Usage

### Authentication

Linxist API uses JWT to authenticate and authorize requests for a given user.

A user will initially log in using its email and password as login credentials,
then will receive a unique token that must be used in every HTTP request, placed
in the `Authorization` header.

The token has a lifespan of **15 minutes**, after which time will be revoked.
It's up to the API user to periodically ask for a new token while using the API, 
by using a "refresh token" to request the new access token.

Let's see an example of the whole process in `curl`, to make it more clear:

1. User logs in into the system by submitting the `email` and `password` to the
   `/api/v1/login` endpoint:

    ```shell script
    curl -o --header "Content-Type: application/x-www-form-urlencoded" \
         --request POST \
         -F username="jon@sharklasers.com" \
         -F password="test-password"}' \
         http://127.0.0.1:5000/login
    ```
   
    Response will be like:
    
    ```json
    {
      "bearer": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODUzMzA5NzUsIm5iZiI6MTU4NTMzMDk3NSwianRpIjoiZDU5NzcwOGUtOWNlZi00YzMwLWIxYjEtZGNjMmU5MGI5OWU4IiwiZXhwIjoxNTg1MzMxODc1LCJpZGVudGl0eSI6MSwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.0xVY6vHVO92AeShEBSZ4MBy_9EmWM2l-RApjTnf0HKk", 
      "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODUzMzA5NzUsIm5iZiI6MTU4NTMzMDk3NSwianRpIjoiOWMyOTE5YjQtMDE4YS00OGEzLWE1NzgtYWQwMDc1YjFhMDYzIiwiZXhwIjoxNTg3OTIyOTc1LCJpZGVudGl0eSI6MSwidHlwZSI6InJlZnJlc2gifQ.E2HU8mM5xt6gP0iK2fdf43YzhLLQs_aQYvTAGrsB7lw"
    }
    ```
   
2. Requests to protected endpoints can now be made by adding the `Authentication`
   header:
   
   ```shell script
   curl --header "Content-Type: application/json" \
        --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODUzMzA5NzUsIm5iZiI6MTU4NTMzMDk3NSwianRpIjoiOWMyOTE5YjQtMDE4YS00OGEzLWE1NzgtYWQwMDc1YjFhMDYzIiwiZXhwIjoxNTg3OTIyOTc1LCJpZGVudGl0eSI6MSwidHlwZSI6InJlZnJlc2gifQ.E2HU8mM5xt6gP0iK2fdf43YzhLLQs_aQYvTAGrsB7lw" \
        http://127.0.0.1:5000/api/v1/records
   ```
